package bbw.ch;

public class Main {

    public static void main(String[] args) {
        Triangle t = new Triangle(10);
        t.draw();

        Square square = new Square(4);
        square.draw();

        Rectangle rectangle = new Rectangle(2, 5);
        rectangle.draw();

        Triangle_with_Size ts = new Triangle_with_Size(5);
        ts.draw(); //draw func not done

        Diamond diamond = new Diamond(6);
        diamond.draw(); //draw func not done
    }
}
